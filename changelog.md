
v0.7.1 / 2015-12-17
==================

  * fix bug in user profile generation before sending it to recsys
  * change headline text of track view
  * improve problem with android FF player height
  * remove global_config.js

v0.7 / 2015-12-17
==================

  * add search to track view (#517) and add a refresh tracks button (#518)
  * use progress bar instead of red alert (#515)
  * use same rating system on all views (#519)
  * create directive for questionnaire
  * load front end config from api (#513)
  * view info when there are no recommendations (#520)
  * call recsys api on profile delete (#509)
  * store regex in config file
  * rename sign up
  * improve cover image selection (#511)
  * store url in recsys performance log
  * use context from recsys api (#508)

v0.6 / 2015-12-07
==================

  * improve info box messages
  * fix recs caching bug
  * add link on cover img to detail view
  * keep sockets alive
  * add config gen and create one central config file
  * change loading sign
  * update cache appropriate
  * speedup spotify requests
  * take logs of lookup time
  * do not reload on back click if in detailed view (#505)
  * fix layout in track view
  * track view does not stop player on dislike (#504)
  * layout problem in track box (#502)
  * do more task async
  * fetch tracks parallel

v0.5.2 / 2015-11-29
==================

  * fix visited color of primary button

v0.5.1 / 2015-11-29
==================

  * fix buttons visited color and separate bootstrap-theme
  * fix looses login on page reload (#503)

v0.5 / 2015-11-28
==================

  * save recApiPassword in separate file (not in git #455)
  * improve spotify lookup speed by increasing the number of used sockets
  * ensure that questionnaire gender value is independent from language
  * fix bug in recommendation fetching routine
  * change look and feel to fit better for the logo
  * design a main page (#454)
  * improve HTML structure of the application
  * create and add logo
  * add possibility to change questionnaire (#453)
  * improve responsive design (#457)
  * add user track profile view (#447)
  * fix mobile not possible to click into the signup form (#456)
  * add cache for spotify track api (using redis)

v0.4 / 2015-09-14
==================

  * implement track selector info boxes as traffic light
  * fetch 30 tracks for track selector and fetch a new recommendation when the user likes a track
  * do not store profileThreshold value in db on update calls if not already set

v0.3 / 2015-09-11
==================

  * fix artist list view in track details
  * add log entry if preview_url is not available and create a directive for the audio preview
  * change rec api call to send full profile with rating values
  * add log when preview is started, paused or ended
  * fix logging bug in tracks server controller
  * fix logging bug in recommendations server controller
  * add log when a detail view is opened as well as a rating is given
  * add log when a link is clicked (currentliy all spodify links in detail view are observed)
  * fix css warnings
  * fix updateUserProfileEvent listener problem
  * fix user profile updates to work
  * add request and response logging
  * fix user profile threshold update in core.server.controller
  * add possibility to delete user(track)profile
  * use threshold to map star rating to thumb rating
  * use user threshold for recommendations (uses default if not set)
  * add profileThreshold to user profile (uses default if not set)
  * remove possibility to change username in UI
  * rename done button fixes 422
  * add logging for signup, signin and signout
  * use username in logs instead of user
  * log signin and signout
  * add log module at server side
  * improve authentication test
  * cleanup client side of the app
  * fix user id check for profile updates
  * cleanup server side of the app
  * move rating function to its own service
  * add loading animations fixes 419 390
  * add tests for tracks controller and fix controllers to fulfill them
  * fix bug in tracks controller
  * add recommendation client tests to cover all functions
  * do not center ratingcontainer in detail view
  * write tests for recommendations route and change controller to fulfill them
  * write tests for tracks route and change controller to fulfill them
  * change the way a user profile update is validated
  * fixes needed to fulfill tests
  * add and modify user tests at server side
  * fix 416 center like buttons
  * fix star rating in detail view (now the correct value is used)
  * write tests and remove some unnecessary code
  * update addedAt in profile as well

v0.2 / 2015-08-31
=================

  * store rating in db
  * add links for tracks and recommendations
  * change page redirect after login
  * update user profile representation in db
  * use context (from frontend) for recommendation api requests
  * fix order of recommendations
  * add detail view
  * sort out all Spotify id's that do not match request type
  * add internationalization to angular.js

v0.1 / 2015-08-19
=================

  * add track selector after login
  * update user schema and add questionnaire to login form
  * add recommendations api to get a list of recommendations
  * add tracks api to get track info
  * add a context switch dropdown menu
  * add mean.js
  * Add readme
